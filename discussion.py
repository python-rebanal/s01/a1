# [Section] Comments 
# Comments in Python are done using "ctrl+/" or # symbol


#[Section] Python Syntax
# Hello World in Python


print("Hello World!") 

# [Section] Indentation
# Where in other programming languages the indentation in code is for readabilityonly, the indentation in Pythonis very important
# In Python, indentaiton is used to indicate a block of code.
# Similar to JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the container of our data
# In Python, a variable name and assigning a value using the equality symbol.


# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter  (A to Z or a to z), dollar sign or underscore.
#after the first character, identifier can have any combination of characters.
#Unlike Javascript that uses the camel casing, Python uses the snake case convention for variables as defined in the PEP (Python Enhacement Proposa).

age = 35
middle_initial = "C"

# Python allows 
name1 , name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name1)


# [Section] Data Type
# Data types convey what kind of information a variable holds. There are different data types and each has its own use.

# In Python, there are commonly used data types:
# 1. String(str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers (int, float, complex) - for integers, decimal and complex numbers.
num_of_days = 365.1 # this is an integer
pi_approx = 3.1416 # this is a float
complex_num = 1 + 5j #this is a complex number, letter j represents the imaginary number

print(type(complex_num))

# 3. Boolean(bool) - for truth values
# Boolean values in Python starts with uppercase letters

isLearning = True
isDifficult = False

# [Section] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

print ("My name is " + full_name)

# [Section] Terminal Outputs
# In Python, printing in the terminal usesthe print() function
#To use variables, concatinate ( + symbol) between strings can be used
#However

#print("My age is " + age)

# [Section] Typecasting
# here are some functions that can be use in typecastins
#1. int() - convert the value into an integer
print(int(3.75))
#2. float() - converts the value into an integer value
print(float(5))
#3. str() - converts the value into string


print("My age is " + str(age))

print(f"Hi my name is {full_name} and my age is {age}")

# [Section] Operations
# Python has operator families that can be used to manipulate variables

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators - used to assign to variables

num1 = 4
num1 += 3
num2=4
print(num1)

# other operators -=, *=, /=, %=

#Comparison operators - used compare values(boolean values)

print (1=="1")

# other operators, !=, >=, <=, > , <

# Logical Operators - use to combine conditional statements

# Logical Or operator
print(True or False and False)

#Logical And operator
print(True and False)

#Logical Not operator
print(not False)
print(num1+num2)

